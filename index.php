<?php
include_once'vendor/autoload.php';

use Pondit\Calculator\NumberCalculator\Addition;
use Pondit\Calculator\NumberCalculator\Subtraction;
use Pondit\Calculator\NumberCalculator\Multiplication;
use Pondit\Calculator\NumberCalculator\Division;

echo "<h1 style='text-align: center'>NumberCalculator</h1>";


$addition = new Addition("10","20");
echo"<b>Addition:</b> ". $addition->addition()."<br/>";

$subtraction= new Subtraction("50","20");
echo "<b>Subtraction: </b>".$subtraction->subtraction()."<br/>";

$multiplication= new Multiplication("5","5");
echo "<b>Multiplication: </b>".$multiplication->multiplication()."<br/>";

$division= new Division("35","5");
echo "<b>Division: </b>".$division->division();







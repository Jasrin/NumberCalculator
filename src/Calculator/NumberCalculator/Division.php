<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 3/25/2018
 * Time: 11:14 PM
 */

namespace Pondit\Calculator\NumberCalculator;


class Division
{
    public $number1;
    public $number2;
    public function __construct($n1,$n2)
    {
        $this->number1 = $n1;
        $this->number2 = $n2;
    }

    public function division(){

        $result =  $this->number1 /  $this->number2 ;
        return $result;
    }

}